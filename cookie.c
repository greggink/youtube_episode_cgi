#include "stdio.h"
#include "stdlib.h"

int main(){

	printf("Content-Type: text/html; charset=utf-8\r\nSet-Cookie: Hello, Cookie here!\r\n\r\n");

	printf("<h3>Hello World!</h3>");

	char *cookie = getenv("HTTP_COOKIE");
	if(cookie){
		printf("The cookie is: <b>%s</b><br>", cookie);
	}else{
		printf("No cookie!");
	}

	return 0;

}//main*/
